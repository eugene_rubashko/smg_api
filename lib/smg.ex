defmodule SMG do
  @moduledoc """
  Elixir client for iTechArt SMG API. Provides corresponding methods and structures.
  """

  use Application

  def start(_type, _args) do
    children = [
      SMG.Cache
    ]

    opts = [strategy: :one_for_one, name: __MODULE__]
    Supervisor.start_link(children ++ env_children(Mix.env()), opts)
  end

  defp env_children(:test), do: []
  defp env_children(_), do: []
end
