defmodule SMG.Request do
  use Ecto.Schema
  alias Ecto.Changeset
  alias SMG.Utils
  alias __MODULE__

  @required_params ~w(employee_id employee_name employee_name_en start_date
    end_date start_time end_time vacation_type_id vacation_type image
    department_name full_day? status_id)a

  @type t :: %Request{}

  @primary_key false
  embedded_schema do
    field(:employee_id, :integer)
    field(:employee_name, :string)
    field(:employee_name_en, :string)
    field(:start_date, :date)
    field(:end_date, :date)
    field(:start_time, :time)
    field(:end_time, :time)
    field(:vacation_type_id, :integer)
    field(:vacation_type, :string)
    field(:image, :string)
    field(:department_name, :string)
    field(:full_day?, :boolean)
    field(:status_id, :integer)
  end

  @spec changeset(t(), map()) :: Changeset.t()
  def changeset(%Request{} = request, params \\ %{}) do
    request
    |> Changeset.cast(params, @required_params)
    |> Changeset.validate_required(@required_params)
  end

  @spec new(map) :: t()
  def new(nil), do: nil

  def new(params) when is_map(params) do
    request_params = %{
      employee_id: params[:ProfileId],
      employee_name: params[:UserName],
      employee_name_en: params[:UserNameEng],
      start_date: params[:StartDate] |> Utils.unix_date_to_date_time(),
      end_date: params[:EndDate] |> Utils.unix_date_to_date_time(),
      start_time: params[:StartTime] |> Utils.str_to_time(),
      end_time: params[:EndTime] |> Utils.str_to_time(),
      vacation_type_id: params[:VacationTypeId],
      vacation_type: params[:VacationType],
      image: params[:Image],
      department_name: params[:DepName],
      full_day?: params[:AllDayEvent],
      status_id: params[:StatusId]
    }

    %Request{}
    |> changeset(request_params)
    |> Changeset.apply_changes()
  end
end
