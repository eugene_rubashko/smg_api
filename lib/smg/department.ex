defmodule SMG.Department do
  use Ecto.Schema
  alias Ecto.Changeset
  alias __MODULE__

  @required_params ~w(id code name employees_number)a

  @type t :: %Department{}

  @primary_key {:id, :integer, autogenerate: false, source: :id}
  embedded_schema do
    field(:code, :string)
    field(:name, :string)
    field(:employees_number, :integer)
  end

  @spec changeset(t(), map()) :: Changeset.t()
  def changeset(%Department{} = department, params \\ %{}) do
    department
    |> Changeset.cast(params, @required_params)
    |> Changeset.validate_required(@required_params)
  end

  @spec new(map) :: t()
  def new(nil), do: nil

  def new(params) when is_map(params) do
    department_params = %{
      id: params |> get_in([:Id]),
      code: params |> get_in([:DepCode]),
      name: params |> get_in([:Name]),
      users_number: params |> get_in([:NumUsers])
    }

    %Department{}
    |> changeset(department_params)
    |> Changeset.apply_changes()
  end
end
