defmodule SMG.Employee do
  use Ecto.Schema
  alias Ecto.Changeset
  alias SMG.Utils
  alias __MODULE__

  @required_params ~w(id first_name last_name first_name_en last_name_en room
    department_id image is_enabled)a
  @optional_params ~w(rank group middle_name birthday skype phone email domain_name)a

  @type t :: %Employee{}

  @primary_key {:id, :integer, autogenerate: false, source: :id}
  embedded_schema do
    field(:first_name, :string)
    field(:last_name, :string)
    field(:first_name_en, :string)
    field(:last_name_en, :string)
    field(:rank, :string)
    field(:room, :string)
    field(:department_id, :integer)
    field(:group, :string)
    field(:image, :string)
    field(:is_enabled, :boolean)
    field(:middle_name, :string)
    field(:birthday, :date)
    field(:skype, :string)
    field(:phone, :string)
    field(:email, :string)
    field(:domain_name, :string)
  end

  @spec changeset(t(), map()) :: Changeset.t()
  def changeset(%Employee{} = employee, params \\ %{}) do
    employee
    |> Changeset.cast(params, @required_params ++ @optional_params)
    |> Changeset.validate_required(@required_params)
  end

  @spec new(map) :: t()
  def new(nil), do: nil

  def new(params) when is_map(params) do
    employee_params = %{
      id: params[:ProfileId],
      first_name: params[:FirstName],
      last_name: params[:LastName],
      first_name_en: params[:FirstNameEng],
      last_name_en: params[:LastNameEng],
      room: params[:Room],
      department_id: params[:DeptId],
      group: params[:Group],
      image: params[:Image],
      is_enabled: params[:IsEnabled],
      rank: params[:Rank],
      middle_name: params[:MiddleName],
      birthday: params[:Birthday] |> Utils.unix_date_to_date_time(),
      skype: params[:Skype],
      phone: params[:Phone],
      email: params[:Email],
      domain_name: params[:DomenName]
    }

    %Employee{}
    |> changeset(employee_params)
    |> Changeset.apply_changes()
  end
end
