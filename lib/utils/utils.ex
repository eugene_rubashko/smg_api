defmodule SMG.Utils do
  @moduledoc false

  use OK.Pipe

  @doc "Converts \"Date(1543536000000+0300)\" to ~D[2018-11-30]"
  def unix_date_to_date_time(nil), do: nil

  def unix_date_to_date_time(date) do
    Regex.named_captures(~r/Date\((?<unix_time>.*)000+/, date)["unix_time"]
    |> str_to_int
    |> DateTime.from_unix()
    ~>> DateTime.to_date()
  end

  @doc "Converts \"09:00\" to to ~T[09:00:00]"
  def str_to_time(str) do
    [hour, min] = str |> String.split(":") |> Enum.map(&str_to_int/1)
    with {:ok, time} <- Time.new(hour, min, 0), do: time
  end

  @doc "Converts #DateTime<2018-11-30 12:12:12.556748Z> to \"11/30/2018 12:12:12\""
  def format_date_time(%DateTime{} = date_time) do
    %{year: year, month: mon, day: day, hour: hour, minute: min, second: sec} = date_time
    "#{mon}/#{day}/#{year} #{hour}:#{min}:#{sec}"
  end

  @doc "Converts string to integer"
  defp str_to_int(str) do
    with {int, _} <- Integer.parse(str), do: int
  end
end
