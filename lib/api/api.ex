defmodule SMG.API do
  @moduledoc """
  Wrapper over iTechArt SMG API.
  """

  alias SMG.{API, Cache, Utils}
  alias SMG.{Department, Employee, Request}

  use OK.Pipe

  @endpoint "https://smg.itechart-group.com/MobileServiceNew/MobileService.svc"

  @authenticate "Authenticate"
  @post_authenticate "PostAuthenticate"
  @all_departments "GetAllDepartments"
  @all_departments_updated "GetAllDepartmentsUpdated"
  @all_employees "GetAllEmployees"
  @employees_short_info "GetEmployeeShortInfo"
  @employees_by_department_id "GetEmployeesByDeptId"
  @employees_by_department_id_updated "GetEmployeesByDeptIdUpdated"
  @employee_details "GetEmployeeDetails"
  @employee_details_updated "GetEmployeeDetailsUpdated"
  @all_requests "GetAllRequests"
  @request_form "GetRequestForm"
  @all_recipients "GetAllRecipients"
  @all_available_listeners "GetAllAvailableListeners"

  ## Authentication

  @spec authenticate(String.t(), String.t()) :: {:ok, String.t()} | {:error, term()}
  def authenticate(username, password) do
    @authenticate
    |> url(%{username: username, password: password})
    |> API.HTTP.get()
  end

  @spec authenticate_post(String.t(), String.t()) :: {:ok, String.t()} | {:error, term()}
  def authenticate_post(username, password) do
    @post_authenticate
    |> url
    |> API.HTTP.post(%{Username: username, Password: password})
  end

  ## Departments

  @spec all_departments() :: {:ok, [Department.t()]} | {:error, term()}
  def all_departments() do
    @all_departments
    |> request
    ~> Enum.map(&Department.new/1)
  end

  @spec all_departments_updated(DateTime.t()) :: {:ok, [Department.t()]} | {:error, term()}
  def all_departments_updated(%DateTime{} = update_date) do
    @all_departments_updated
    |> request(%{updatedDate: update_date |> Utils.format_date_time()})
    ~> Enum.map(&Department.new/1)
  end

  ## Employees

  @spec all_employees() :: {:ok, [Employee.t()]} | {:error, term()}
  def all_employees() do
    @all_employees
    |> request
    ~> Enum.map(&Employee.new/1)
  end

  @spec employees_short_info(boolean(), DateTime.t()) :: {:ok, [Employee.t()]} | {:error, term()}
  def employees_short_info(enabled?, %DateTime{} = update_date) when is_boolean(enabled?) do
    @employees_short_info
    |> request(%{initialRequest: enabled?, updatedDate: update_date |> Utils.format_date_time()})
    ~> Enum.map(&Employee.new/1)
  end

  @spec employees_by_department_id(integer() | String.t()) ::
          {:ok, [Employee.t()]} | {:error, term()}
  def employees_by_department_id(department_id) do
    @employees_by_department_id
    |> request(%{departmentId: "#{department_id}"})
    ~> Enum.map(&Employee.new/1)
  end

  @spec employees_by_department_id_updated(integer() | String.t(), DateTime.t()) ::
          {:ok, [Employee.t()]} | {:error, term()}
  def employees_by_department_id_updated(department_id, %DateTime{} = update_date) do
    @employees_by_department_id_updated
    |> request(%{
      departmentId: "#{department_id}",
      updatedDate: update_date |> Utils.format_date_time()
    })
    ~> Enum.map(&Employee.new/1)
  end

  @spec employee_details(integer() | String.t()) :: {:ok, Employee.t()} | {:error, term()}
  def employee_details(id) do
    @employee_details
    |> request(%{profileId: "#{id}"})
    ~> Employee.new()
  end

  @spec employee_details_updated(integer() | String.t(), DateTime.t()) ::
          {:ok, Employee.t()} | {:error, term()}
  def employee_details_updated(id, %DateTime{} = update_date) do
    @employee_details_updated
    |> request(%{profileId: "#{id}", updatedDate: update_date |> Utils.format_date_time()})
    ~> Employee.new()
  end

  ## Requests

  @spec all_requests() :: {:ok, [Request.t()]} | {:error, term()}
  def all_requests() do
    @all_requests
    |> request
    ~> Enum.map(&Request.new/1)
  end

  @spec request_form(integer() | String.t()) :: {:ok, map()} | {:error, term()}
  def request_form(vacation_type_id) do
    @request_form
    |> request(%{requestTypeId: "#{vacation_type_id}"})
  end

  ## Recipients

  @spec all_requests() :: {:ok, map()} | {:error, term()}
  def all_recipients() do
    @all_recipients
    |> request
  end

  ## Available listeners

  @spec all_available_listeners() :: {:ok, map()} | {:error, term()}
  def all_available_listeners() do
    @all_available_listeners
    |> request
  end

  ## Variables from config

  def username(), do: Application.get_env(:smg, :username)
  def password(), do: Application.get_env(:smg, :password)

  ### Private functions

  defp request(method, params \\ %{}) do
    case Cache.fetch_session_id() do
      {:error, message} ->
        {:error, message}

      {:ok, session_id} ->
        case API.HTTP.get(url(method, %{sessionId: session_id} |> Map.merge(params))) do
          {:ok, _} = response ->
            response

          {:error, message} ->
            cond do
              is_binary(message) && String.match?(message, ~r/no session/i) ->
                Cache.update_session_id()
                request(method)

              true ->
                {:error, message}
            end
        end
    end
  end

  defp url(method, params \\ %{}) do
    "#{@endpoint}/#{method}?#{URI.encode_query(params)}"
  end
end
