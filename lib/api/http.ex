defmodule SMG.API.HTTP do
  @moduledoc """
  Auxiliary module for sending HTTP requests.
  """

  require Logger

  @doc "Sends GET request to `url`."
  def get(url) do
    url
    |> HTTPoison.get([], hackhey: [pool: :default])
    |> process_response
  end

  @doc "Sends POST request to `url` with `body`."
  def post(url, body) do
    url
    |> HTTPoison.post(body |> Jason.encode!(), [{"Content-Type", "application/json"}])
    |> process_response
  end

  ### Private functions

  defp process_response(response) do
    with {:ok, %{body: body}} <- response,
         {:ok, payload} <- Jason.decode(body, keys: :atoms) do
      payload |> process_payload
    else
      {:error, _} = error ->
        Logger.warn(inspect(error))
        {:error, :failed_request}
    end
  end

  defp process_payload(%{ErrorCode: ""} = payload), do: payload |> process_body
  defp process_payload(%{ErrorCode: message}), do: {:error, message}

  defp process_body(%{SessionId: session_id}), do: {:ok, session_id}
  defp process_body(%{Depts: departments}), do: {:ok, departments}
  defp process_body(%{Profiles: profiles}), do: {:ok, profiles}
  defp process_body(%{Profile: profile}), do: {:ok, profile}
  defp process_body(%{Requests: requests}), do: {:ok, requests}
  defp process_body(%{Recipients: recipients}), do: {:ok, recipients}
  defp process_body(%{Items: items}), do: {:ok, items}
  defp process_body(%{Form: form}), do: {:ok, form}
end
