defmodule SMG.Cache do
  @moduledoc """
  Cache module to store Session ID.
  """

  use Agent
  alias SMG.API

  @doc false
  def start_link(_) do
    Agent.start_link(fn -> API.authenticate_post(API.username(), API.password()) end,
      name: __MODULE__
    )
  end

  @doc "Gets Session ID."
  @spec get_session_id() :: {:ok, integer()} | {:error, term()}
  def get_session_id() do
    Agent.get(__MODULE__, fn state -> state end)
  end

  @doc "Updates Session ID."
  @spec update_session_id() :: :ok
  def update_session_id() do
    :ok =
      Agent.update(
        __MODULE__,
        fn _ -> API.authenticate_post(API.username(), API.password()) end
      )
  end

  @doc "Updates Session ID if necessary and gets it."
  @spec fetch_session_id() :: {:ok, integer()} | {:error, term()}
  def fetch_session_id() do
    case get_session_id() do
      {:ok, session_id} ->
        {:ok, session_id}

      {:error, _} ->
        update_session_id()
        get_session_id()
    end
  end
end
