# SMG API

**Elixir client for iTechArt SMG API. Provides corresponding methods and structures.**

## Configuration

Put necessary username and password to `config/secrets.exs` of your application:

```elixir
config :smg,
  username: "yauheni.rubashka",
  password: "$yPeeR_$eKReT;)"
```
