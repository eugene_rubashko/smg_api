defmodule SMG.MixProject do
  use Mix.Project

  def project do
    [
      app: :smg,
      version: "0.1.2",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {SMG, []}
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 1.4"},
      {:jason, "~> 1.1"},
      {:ecto, "~> 3.0"},
      {:ok, "~> 2.0"}
    ]
  end
end
