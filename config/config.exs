use Mix.Config

if File.exists?(Path.join([Path.dirname(__ENV__.file), "secrets.exs"])) do
  import_config "secrets.exs"
end
